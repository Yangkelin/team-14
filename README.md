* # README #
* 
* 
*  ## Team 14   SKIPPER
*  
*  
*  ## Team members:  Chang Liu, Biqiu Li,   Wenxiao He, Yangkelin Wei, Yingyi Yang
*  
*  
*  ## Project Description:  This project implements c4.5 algorithm to build up a decision tree and *make cross validation to get accuracy.
*  
*  
*  ## Usage:  run Main.java with file name as parameters 
*  
*  
*  ## Reference Links: 
*  http://blog.csdn.net/yangliuy/article/details/7322015
*  
*